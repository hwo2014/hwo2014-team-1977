package noobbot;

import noobbot.json.JsonProcessor;

import java.io.IOException;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port);

        Communicator communicator = new Communicator();
        communicator.connect(host, port);

        new Main(communicator, botName, botKey);
    }

    public Main(final Communicator communicator, String botName, String botKey) throws IOException {
        System.out.println("Joining as " + botName + "/" + botKey);
        communicator.join(botName, botKey);

        JsonProcessor processor = new JsonProcessor(communicator);
        String line;
        while ((line = communicator.readLine()) != null) {
            processor.processJson(line);
        }
        System.out.println("Exiting");
    }

}

