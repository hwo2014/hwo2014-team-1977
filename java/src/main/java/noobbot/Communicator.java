package noobbot;

import noobbot.messages.Join;
import noobbot.messages.Message;

import java.io.*;
import java.net.Socket;

public class Communicator {
    private PrintWriter writer;
    private BufferedReader reader;

    public void connect(String host, int port) throws IOException {
        final Socket socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
    }

    public void send(final Message msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    public void join(String botName, String botKey) {
        send(new Join(botName, botKey));
    }

    public String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
