package noobbot.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import noobbot.Communicator;
import noobbot.messages.Ping;
import noobbot.messages.Throttle;
import noobbot.messages.incoming.*;

public class JsonProcessor {
    final Gson gson;
    private Communicator communicator;
    private int lastPiece = 0;

    public JsonProcessor(Communicator communicator) {
        this.communicator = communicator;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(IncomingMessage.class, new IncomingMessageDeserializer());
        gson = gsonBuilder.create();
    }

    public void processJson(String line) {
        IncomingMessage incomingMessage = parseJson(line);
        if (incomingMessage instanceof CarPositionMessage) {
            CarPositionMessage carMessage = (CarPositionMessage) incomingMessage;
            int piece = carMessage.positions().get(0).pieceIndex();
            if (piece != lastPiece) {
                lastPiece = piece;
                System.out.println(piece);
            }
            double angle = carMessage.positions().get(0).angle();
            if (angle != 0.0) {
                communicateSpeed(0.6);
            } else {
                communicateSpeed(0.8);
            }
        } else if (incomingMessage instanceof JoinMessage) {
            System.out.println("Joined");
            ping();
        } else if (incomingMessage instanceof GameInitMessage) {
            System.out.println("Race init");
            ping();
        } else if (false) {
            System.out.println("Race end");
            ping();
        } else if (incomingMessage instanceof GameStartMessage) {
            System.out.println("Race start");
            ping();
        } else if (incomingMessage instanceof CarMessage) {
            System.out.println(((CarMessage) incomingMessage).color());
            ping();
        } else {
            System.out.println("Unknown json:\n\t" + line);
            ping();
        }
    }

    private void ping() {
        communicator.send(new Ping());
    }

    private void communicateSpeed(double value) {
        communicator.send(new Throttle(value));
    }

    public IncomingMessage parseJson(String joinJson) {
        return gson.fromJson(joinJson, IncomingMessage.class);
    }
}
