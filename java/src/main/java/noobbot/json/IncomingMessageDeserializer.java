package noobbot.json;

import com.google.gson.*;
import noobbot.messages.incoming.*;
import noobbot.racing.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IncomingMessageDeserializer implements JsonDeserializer<IncomingMessage> {

    @Override
    public IncomingMessage deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        JsonElement jsonType = jsonObject.get("msgType");
        String type = jsonType.getAsString();

        IncomingMessage typeModel = null;

        switch (type) {
            case "join": {
                JsonObject data = dataAsObject(jsonObject);
                typeModel = new JoinMessage()
                        .name(data.get("name").getAsString())
                        .key(data.get("key").getAsString());
                break;
            }
            case "yourCar": {
                JsonObject data = dataAsObject(jsonObject);
                typeModel = new CarMessage()
                        .name(data.get("name").getAsString())
                        .color(data.get("color").getAsString());
                break;
            }
            case "gameInit": {
                JsonObject data = dataAsObject(jsonObject);
                typeModel = gameInitMessage(data);
                break;
            }
            case "gameStart": {
                typeModel = new GameStartMessage();
                break;
            }
            case "carPositions": {
                JsonArray data = jsonObject.get("data").getAsJsonArray();
                CarPositionMessage message = new CarPositionMessage();
                for (JsonElement jsonElement : data) {
                    JsonObject positionElement = jsonElement.getAsJsonObject();
                    message.withPosition(getCarPosition(positionElement));
                }
                typeModel = message;
                break;
            }

        }

        return typeModel;
    }

    private CarPosition getCarPosition(JsonObject positionElement) {
        JsonObject piecePosition = positionElement.get("piecePosition").getAsJsonObject();
        JsonObject lane = piecePosition.get("lane").getAsJsonObject();

        String color = positionElement.get("id").getAsJsonObject().get("color").getAsString();
        double angle = positionElement.get("angle").getAsDouble();
        int pieceIndex = piecePosition.get("pieceIndex").getAsInt();
        double inPieceDistance = piecePosition.get("inPieceDistance").getAsDouble();
        int startLaneIndex = lane.get("startLaneIndex").getAsInt();
        int endLaneIndex = lane.get("endLaneIndex").getAsInt();
        int lap = piecePosition.get("lap").getAsInt();
        return new CarPosition()
                .carColor(color)
                .angle(angle)
                .pieceIndex(pieceIndex)
                .inPieceDistance(inPieceDistance)
                .laneStart(startLaneIndex)
                .laneEnd(endLaneIndex)
                .lap(lap);
    }

    private GameInitMessage gameInitMessage(JsonObject data) {
        GameInitMessage gameInitMessage = new GameInitMessage();
        JsonObject race = data.get("race").getAsJsonObject();
        gameInitMessage.cars(getCars(race));
        gameInitMessage.track(getTrack(race));
        gameInitMessage.raceSession(getRaceSession(race));

        return gameInitMessage;
    }

    private RaceSession getRaceSession(JsonObject race) {
        RaceSession raceSession = new RaceSession();
        JsonObject raceSessionObject = race.get("raceSession").getAsJsonObject();
        raceSession.laps(raceSessionObject.get("laps").getAsInt());
        raceSession.maxLapTime(raceSessionObject.get("maxLapTimeMs").getAsInt());
        if (raceSessionObject.has("quickRace") && raceSessionObject.get("quickRace").getAsBoolean()) {
            raceSession.quickRace();
        }
        return raceSession;
    }

    private Track getTrack(JsonObject race) {
        JsonObject trackObject = race.get("track").getAsJsonObject();
        Track track = new Track();
        addPieces(trackObject, track);
        JsonArray lanes = trackObject.get("lanes").getAsJsonArray();
        for (JsonElement jsonElement : lanes) {
            JsonObject laneObject = jsonElement.getAsJsonObject();
            track.withLane(getLane(laneObject));
        }

        return track;
    }

    private Lane getLane(JsonObject laneObject) {
        Lane lane = new Lane();
        lane.distance(laneObject.get("distanceFromCenter").getAsInt())
                .index(laneObject.get("index").getAsInt());
        return lane;
    }

    private void addPieces(JsonObject trackObject, Track track) {
        JsonArray pieces = trackObject.get("pieces").getAsJsonArray();
        for (JsonElement jsonElement : pieces) {
            JsonObject pieceObject = jsonElement.getAsJsonObject();
            track.withPiece(getPiece(pieceObject));
        }
    }

    private Piece getPiece(JsonObject pieceObject) {
        Piece piece = new Piece();
        if (pieceObject.has("length")) {
            piece.length(pieceObject.get("length").getAsDouble());
        }
        if (pieceObject.has("radius")) {
            piece.radius(pieceObject.get("radius").getAsDouble());
        }
        if (pieceObject.has("angle")) {
            piece.angle(pieceObject.get("angle").getAsDouble());
        }
        if (pieceObject.has("switch") && pieceObject.get("switch").getAsBoolean()) {
            piece.withSwitch();
        }
        return piece;
    }

    private List<Car> getCars(JsonObject race) {
        JsonArray cars = race.get("cars").getAsJsonArray();
        List<Car> carsList = new ArrayList<>();
        for (JsonElement jsonObject : cars) {
            JsonObject car = jsonObject.getAsJsonObject();
            JsonObject dimensions = car.get("dimensions").getAsJsonObject();
            JsonObject id = car.get("id").getAsJsonObject();
            carsList.add(new Car().color(id.get("color").getAsString())
                    .name(id.get("name").getAsString())
                    .dimensions(dimensions.get("length").getAsDouble(), dimensions.get("width").getAsDouble(), dimensions.get("guideFlagPosition").getAsDouble()));
        }
        return carsList;
    }

    private JsonObject dataAsObject(JsonObject jsonObject) {
        return jsonObject.get("data").getAsJsonObject();
    }
}