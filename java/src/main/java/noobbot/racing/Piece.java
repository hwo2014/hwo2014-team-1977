package noobbot.racing;

public class Piece {
    private double length;
    private boolean hasSwitch;
    private double angle;
    private double radius;

    public Piece length(double length) {
        this.length = length;
        return this;
    }

    public Piece withSwitch() {
        hasSwitch = true;
        return this;
    }

    public Piece angle(double angle) {
        this.angle = angle;
        return this;
    }

    public Piece radius(double radius) {
        this.radius = radius;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Piece piece = (Piece) o;

        if (Double.compare(piece.angle, angle) != 0) return false;
        if (hasSwitch != piece.hasSwitch) return false;
        if (Double.compare(piece.length, length) != 0) return false;
        if (Double.compare(piece.radius, radius) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(length);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (hasSwitch ? 1 : 0);
        temp = Double.doubleToLongBits(angle);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(radius);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Piece{" +
                "length=" + length +
                ", hasSwitch=" + hasSwitch +
                ", angle=" + angle +
                ", radius=" + radius +
                '}';
    }
}
