package noobbot.racing;

import java.util.ArrayList;
import java.util.List;

public class Track {
    private List<Piece> pieces = new ArrayList<>();
    private List<Lane> lanes = new ArrayList<>();

    public Track withPiece(Piece piece) {
        pieces.add(piece);
        return this;
    }

    public Track withLane(Lane lane) {
        lanes.add(lane);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Track track = (Track) o;

        if (lanes != null ? !lanes.equals(track.lanes) : track.lanes != null) return false;
        if (pieces != null ? !pieces.equals(track.pieces) : track.pieces != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pieces != null ? pieces.hashCode() : 0;
        result = 31 * result + (lanes != null ? lanes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Track{" +
                "pieces=" + pieces +
                ", lanes=" + lanes +
                '}';
    }
}
