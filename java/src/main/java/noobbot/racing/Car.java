package noobbot.racing;

public class Car {
    private String name;
    private String color;
    private CarDimensions dimensions;

    public Car name(String name) {
        this.name = name;
        return this;
    }

    public Car color(String color) {
        this.color = color;
        return this;
    }

    public Car dimensions(double length, double width, double guideFlagPosition) {
        this.dimensions = new CarDimensions(length, width, guideFlagPosition);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (color != null ? !color.equals(car.color) : car.color != null) return false;
        if (dimensions != null ? !dimensions.equals(car.dimensions) : car.dimensions != null) return false;
        if (name != null ? !name.equals(car.name) : car.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (dimensions != null ? dimensions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", dimensions=" + dimensions +
                '}';
    }
}
