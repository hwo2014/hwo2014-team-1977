package noobbot.racing;

public class RaceSession {
    private int laps;
    private int maxLapTime;
    private boolean quickRace;

    public RaceSession laps(int laps) {
        this.laps = laps;
        return this;
    }

    public RaceSession maxLapTime(int maxLapTime) {
        this.maxLapTime = maxLapTime;
        return this;
    }

    public RaceSession quickRace() {
        quickRace = true;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RaceSession that = (RaceSession) o;

        if (laps != that.laps) return false;
        if (maxLapTime != that.maxLapTime) return false;
        if (quickRace != that.quickRace) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = laps;
        result = 31 * result + maxLapTime;
        result = 31 * result + (quickRace ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RaceSession{" +
                "laps=" + laps +
                ", maxLapTime=" + maxLapTime +
                ", quickRace=" + quickRace +
                '}';
    }
}
