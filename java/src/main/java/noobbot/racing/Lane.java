package noobbot.racing;

public class Lane {
    private int distance;
    private int index;

    public Lane distance(int distance) {
        this.distance = distance;
        return this;
    }

    public Lane index(int index) {
        this.index = index;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lane lane = (Lane) o;

        if (distance != lane.distance) return false;
        if (index != lane.index) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = distance;
        result = 31 * result + index;
        return result;
    }

    @Override
    public String toString() {
        return "Lane{" +
                "distance=" + distance +
                ", index=" + index +
                '}';
    }
}
