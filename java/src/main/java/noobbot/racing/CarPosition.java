package noobbot.racing;

public class CarPosition {
    private String color;
    private double angle;
    private int pieceIndex;
    private double inPieceDistance;
    private int laneStart;
    private int laneEnd;
    private int lap;

    public CarPosition carColor(String color) {
        this.color = color;
        return this;
    }

    public CarPosition angle(double angle) {
        this.angle = angle;
        return this;
    }

    public CarPosition pieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
        return this;
    }


    public CarPosition inPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
        return this;
    }

    public CarPosition laneStart(int laneStart) {
        this.laneStart = laneStart;
        return this;
    }

    public CarPosition laneEnd(int laneEnd) {
        this.laneEnd = laneEnd;
        return this;
    }

    public CarPosition lap(int lap) {
        this.lap = lap;
        return this;
    }

    public int pieceIndex() {
        return pieceIndex;
    }

    public double angle() {
        return angle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarPosition that = (CarPosition) o;

        if (Double.compare(that.angle, angle) != 0) return false;
        if (Double.compare(that.inPieceDistance, inPieceDistance) != 0) return false;
        if (laneEnd != that.laneEnd) return false;
        if (laneStart != that.laneStart) return false;
        if (lap != that.lap) return false;
        if (pieceIndex != that.pieceIndex) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = color != null ? color.hashCode() : 0;
        temp = Double.doubleToLongBits(angle);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + pieceIndex;
        temp = Double.doubleToLongBits(inPieceDistance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + laneStart;
        result = 31 * result + laneEnd;
        result = 31 * result + lap;
        return result;
    }

    @Override
    public String toString() {
        return "CarPosition{" +
                "color='" + color + '\'' +
                ", angle=" + angle +
                ", pieceIndex=" + pieceIndex +
                ", inPieceDistance=" + inPieceDistance +
                ", laneStart=" + laneStart +
                ", laneEnd=" + laneEnd +
                ", lap=" + lap +
                '}';
    }
}
