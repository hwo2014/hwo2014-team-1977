package noobbot.racing;

public class CarDimensions {
    private final double length;
    private final double width;
    private final double guideFlagPosition;

    public CarDimensions(double length, double width, double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarDimensions that = (CarDimensions) o;

        if (Double.compare(that.guideFlagPosition, guideFlagPosition) != 0) return false;
        if (Double.compare(that.length, length) != 0) return false;
        if (Double.compare(that.width, width) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(length);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(width);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(guideFlagPosition);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "CarDimensions{" +
                "length=" + length +
                ", width=" + width +
                ", guideFlagPosition=" + guideFlagPosition +
                '}';
    }
}
