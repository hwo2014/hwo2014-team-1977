package noobbot.messages.incoming;

import noobbot.racing.CarPosition;

import java.util.ArrayList;
import java.util.List;

public class CarPositionMessage implements IncomingMessage {
    private List<CarPosition> positions = new ArrayList<>();

    public CarPositionMessage withPosition(CarPosition carPosition) {
        positions.add(carPosition);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarPositionMessage that = (CarPositionMessage) o;

        if (positions != null ? !positions.equals(that.positions) : that.positions != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return "CarPositionMessage{" +
                "positions=" + positions +
                '}';
    }

    @Override
    public int hashCode() {
        return positions != null ? positions.hashCode() : 0;
    }

    public List<CarPosition> positions() {
        return positions;
    }
}
