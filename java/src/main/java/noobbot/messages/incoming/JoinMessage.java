package noobbot.messages.incoming;

public class JoinMessage implements IncomingMessage {
    private String name;
    private String key;

    public JoinMessage name(String name) {
        this.name = name;
        return this;
    }

    public JoinMessage key(String key) {
        this.key = key;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JoinMessage that = (JoinMessage) o;

        if (key != null ? !key.equals(that.key) : that.key != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "JoinMessage{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
