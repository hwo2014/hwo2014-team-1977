package noobbot.messages.incoming;

public class CarMessage implements IncomingMessage{
    private String name;
    private String color;

    public CarMessage name(String name) {
        this.name = name;
        return this;
    }

    public CarMessage color(String color) {
        this.color = color;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarMessage that = (CarMessage) o;

        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CarMessage{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public String color() {
        return color;
    }
}
