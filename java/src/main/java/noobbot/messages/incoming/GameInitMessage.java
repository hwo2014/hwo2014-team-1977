package noobbot.messages.incoming;

import noobbot.racing.Car;
import noobbot.racing.RaceSession;
import noobbot.racing.Track;

import java.util.List;

public class GameInitMessage implements IncomingMessage{
    private Track track;
    private List<Car> cars;
    private RaceSession raceSession;

    public GameInitMessage track(Track track) {
        this.track = track;
        return this;
    }

    public GameInitMessage cars(List<Car> cars) {
        this.cars = cars;
        return this;
    }

    public GameInitMessage raceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameInitMessage that = (GameInitMessage) o;

        if (cars != null ? !cars.equals(that.cars) : that.cars != null) return false;
        if (raceSession != null ? !raceSession.equals(that.raceSession) : that.raceSession != null) return false;
        if (track != null ? !track.equals(that.track) : that.track != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = track != null ? track.hashCode() : 0;
        result = 31 * result + (cars != null ? cars.hashCode() : 0);
        result = 31 * result + (raceSession != null ? raceSession.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GameInitMessage{" +
                "track=" + track +
                ", cars=" + cars +
                ", raceSession=" + raceSession +
                '}';
    }

}
