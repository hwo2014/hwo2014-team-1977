package noobbot.messages;

public class Ping extends Message {
    @Override
    protected String msgType() {
        return "ping";
    }
}
