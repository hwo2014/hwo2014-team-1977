package noobbot.messages;

import com.google.gson.Gson;

public abstract class Message {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
