package noobbot.json;

import noobbot.Communicator;
import noobbot.messages.incoming.*;
import noobbot.racing.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class JsonProcessorTest {

    @Mock
    private Communicator communicator;

    @InjectMocks
    JsonProcessor processor;

    @Test
    public void shouldParseJoin() {
        String joinJson = "{\"msgType\": \"join\", \"data\": {\n" +
                "\"name\": \"Schumacher\",\n" +
                "\"key\": \"ABCDEF\"\n" +
                "}}";

        IncomingMessage message = processor.parseJson(joinJson);

        JoinMessage joinMessage = new JoinMessage().name("Schumacher").key("ABCDEF");
        assertThat(message).isInstanceOf(JoinMessage.class).isEqualTo(joinMessage);
    }

    @Test
    public void shouldParseYourCar() {
        String joinJson = "{\"msgType\": \"yourCar\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"color\": \"red\"\n" +
                "}}";

        IncomingMessage message = processor.parseJson(joinJson);

        CarMessage carMessage = new CarMessage().name("Schumacher").color("red");
        assertThat(message).isInstanceOf(CarMessage.class).isEqualTo(carMessage);
    }

    @Test
    public void shouldParseGameInit() {
        String gameInitJson = "{\"msgType\": \"gameInit\", \"data\": {\n" +
                "  \"race\": {\n" +
                "    \"track\": {\n" +
                "      \"id\": \"indianapolis\",\n" +
                "      \"name\": \"Indianapolis\",\n" +
                "      \"pieces\": [\n" +
                "        {\n" +
                "          \"length\": 100.0\n" +
                "        },\n" +
                "        {\n" +
                "          \"length\": 100.0,\n" +
                "          \"switch\": true\n" +
                "        },\n" +
                "        {\n" +
                "          \"radius\": 200,\n" +
                "          \"angle\": 22.5\n" +
                "        }\n" +
                "      ],\n" +
                "      \"lanes\": [\n" +
                "        {\n" +
                "          \"distanceFromCenter\": -20,\n" +
                "          \"index\": 0\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 0,\n" +
                "          \"index\": 1\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 20,\n" +
                "          \"index\": 2\n" +
                "        }\n" +
                "      ],\n" +
                "      \"startingPoint\": {\n" +
                "        \"position\": {\n" +
                "          \"x\": -340.0,\n" +
                "          \"y\": -96.0\n" +
                "        },\n" +
                "        \"angle\": 90.0\n" +
                "      }\n" +
                "    },\n" +
                "    \"cars\": [\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Schumacher\",\n" +
                "          \"color\": \"red\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Rosberg\",\n" +
                "          \"color\": \"blue\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"raceSession\": {\n" +
                "      \"laps\": 3,\n" +
                "      \"maxLapTimeMs\": 30000,\n" +
                "      \"quickRace\": true\n" +
                "    }\n" +
                "  }\n" +
                "}}";

        IncomingMessage message = processor.parseJson(gameInitJson);

        GameInitMessage carMessage = new GameInitMessage()
                .track(new Track()
                        .withPiece(new Piece().length(100.0))
                        .withPiece(new Piece().length(100.0).withSwitch())
                        .withPiece(new Piece().radius(200.0).angle(22.5))
                        .withLane(new Lane().distance(-20).index(0))
                        .withLane(new Lane().distance(0).index(1))
                        .withLane(new Lane().distance(20).index(2)))
                .cars(asList(new Car().name("Schumacher").color("red")
                                .dimensions(40.0, 20.0, 10.0),
                        new Car().name("Rosberg").color("blue")
                                .dimensions(40.0, 20.0, 10.0)
                ))
                .raceSession(new RaceSession().laps(3).maxLapTime(30000).quickRace());

        assertThat(message).isInstanceOf(GameInitMessage.class).isEqualTo(carMessage);
    }

    @Test
    public void shouldParseGameStart() {
        String json = "{\"msgType\": \"gameStart\", \"data\": null}";

        IncomingMessage message = processor.parseJson(json);

        assertThat(message).isInstanceOf(GameStartMessage.class);
    }

    @Test
    public void shouldParseCarPosition() {
        String json = "{\"msgType\": \"carPositions\", \"data\": [\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Schumacher\",\n" +
                "      \"color\": \"red\"\n" +
                "    },\n" +
                "    \"angle\": 0.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 0.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 0,\n" +
                "        \"endLaneIndex\": 0\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Rosberg\",\n" +
                "      \"color\": \"blue\"\n" +
                "    },\n" +
                "    \"angle\": 45.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 20.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 1,\n" +
                "        \"endLaneIndex\": 1\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  }\n" +
                "], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 0}";

        IncomingMessage message = processor.parseJson(json);

        CarPositionMessage carPositionMessage = new CarPositionMessage()
                .withPosition(new CarPosition().carColor("red")
                        .angle(0.0)
                        .pieceIndex(0)
                        .inPieceDistance(0.0)
                        .laneStart(0)
                        .laneEnd(0)
                        .lap(0))
                .withPosition(new CarPosition().carColor("blue")
                        .angle(45.0)
                        .pieceIndex(0)
                        .inPieceDistance(20.0)
                        .laneStart(1)
                        .laneEnd(1)
                        .lap(0));

        assertThat(message).isInstanceOf(CarPositionMessage.class).isEqualTo(carPositionMessage);
    }

    @Test
    public void shouldParseGameEnd() {
        String json = "{\"msgType\": \"gameEnd\", \"data\": {\n" +
                "  \"results\": [\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Schumacher\",\n" +
                "        \"color\": \"red\"\n" +
                "      },\n" +
                "      \"result\": {\n" +
                "        \"laps\": 3,\n" +
                "        \"ticks\": 9999,\n" +
                "        \"millis\": 45245\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Rosberg\",\n" +
                "        \"color\": \"blue\"\n" +
                "      },\n" +
                "      \"result\": {}\n" +
                "    }\n" +
                "  ],\n" +
                "  \"bestLaps\": [\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Schumacher\",\n" +
                "        \"color\": \"red\"\n" +
                "      },\n" +
                "      \"result\": {\n" +
                "        \"lap\": 2,\n" +
                "        \"ticks\": 3333,\n" +
                "        \"millis\": 20000\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"car\": {\n" +
                "        \"name\": \"Rosberg\",\n" +
                "        \"color\": \"blue\"\n" +
                "      },\n" +
                "      \"result\": {}\n" +
                "    }\n" +
                "  ]\n" +
                "}}";

        IncomingMessage message = processor.parseJson(json);

        GameEndMessage endMessage = new GameEndMessage();

//        assertThat(message).isInstanceOf(GameEndMessage.class).isEqualTo(endMessage);
    }
}
