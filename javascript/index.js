var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

var pieces, myColor, currentPiece = -1, currentIndex, throttleValue = 1.0, lastAngle;

function sendThrottle(value) {
    value = Math.max(Math.min(value, 1.0), 0.0);
    console.log("throttle", value);
    send({
        msgType: "throttle",
        data: value
    });
}

function getMyPosition(msg) {
    var myPosition;
    for (var i = 0; i < msg.data.length; i++) {
        if (msg.data[i].id.color == myColor);
        myPosition = msg.data[i];
    }
    return myPosition;
}
function logCurrentPiece(myPosition) {
    if (myPosition.piecePosition.pieceIndex != currentIndex) {
        currentIndex = myPosition.piecePosition.pieceIndex;
        currentPiece = pieces[currentIndex];
        console.log(currentIndex);
        if(currentPiece.length) {
            console.log("l:", currentPiece.length);
        } else {
            console.log("a:", currentPiece.angle);
        }
    }
}
function logCurrentAngle(myPosition) {
    if (myPosition.angle > 0) {
        console.log("\t", myPosition.angle);
    }
}
function pieceOffset(number) {
    return (currentIndex + number) % pieces.length;
}
function angleChange(myPosition) {
    return myPosition.angle - lastAngle;
}
function throttle(myPosition) {
    if (pieces[currentIndex].angle) {
        throttleValue = Math.min(0.6, throttleValue + 0.5);
    } else if (pieces[pieceOffset(1)].angle) {
        throttleValue = Math.min(0.6, throttleValue + 0.5);
    } else if (pieces[pieceOffset(2)].angle) {
        throttleValue = Math.min(0.7, throttleValue + 0.5);
    } else {
        throttleValue = Math.min(0.9, throttleValue + 0.5);
    }

    if(angleChange(myPosition) > 2) {
        throttleValue -= 0.1;
    }

    sendThrottle(throttleValue);
}
jsonStream.on('data', function (msg) {
    if (msg.msgType === 'carPositions') {
        var myPosition = getMyPosition(msg);
        logCurrentPiece(myPosition);
        logCurrentAngle(myPosition);
        throttle(myPosition);

        lastAngle = myPosition.angle;
    } else {
        if (msg.msgType === 'join') {
            console.log('Joined')
        } else if (msg.msgType === 'gameStart') {
            console.log('Race started');
        } else if (msg.msgType === 'gameEnd') {
            console.log('Race ended');
        } else if (msg.msgType === 'gameInit') {
            pieces = msg.data.race.track.pieces;
        } else if (msg.msgType === 'yourCar') {
            myColor = msg.data.color;
        } else {
            console.log("Unknown json:");
            console.log("\t", msg);
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});
